# Solaar
Solaar is a library handling Logitech's Unifying Receiver peripherals. It is able to pair/unpair devices to the receiver.

## Building and Installation

Run `meson` to configure the build environment and then `ninja` to build

    meson build --prefix=/usr
    ninja -C build

To install, use `ninja install`

    sudo ninja install
