/*
 * Copyright 2018 Collabora Ltd. (https://collabora.com)
 *
 * Authored by: Corentin Noël <corentin.noel@collabora.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#define G_LOG_DOMAIN "solaar-device"

#include <glib.h>
#include <glib/gi18n.h>
#include <gio/gio.h>
#include <gudev/gudev.h>

#include "solaar-device.h"
#include "solaar-device-priv.h"
#include "solaar-receiver.h"
#include "solaar-receiver-priv.h"
#include "solaar-enums.h"
#include "solaar-device-descriptors.h"
#include "solaar-constants.h"

struct _SolaarDevice
{
  GObject parent_instance;
  SolaarDevice *receiver;
  guint8 devnumber;
  gchar *wpid;
  gchar *serial;
  guint8 polling_rate;
  const SolaarDeviceDescriptor *descriptor;
};

G_DEFINE_TYPE (SolaarDevice, solaar_device, G_TYPE_OBJECT)

enum
{
  PROP_RECEIVER = 1,
  N_PROPERTIES
};

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL, };

SolaarDevice *
_solaar_device_new (SolaarReceiver *receiver,
                    guint8 devnumber)
{
    g_autoptr(GError) error = NULL;
    g_autoptr(SolaarDevice) device = NULL;
    g_autofree gchar *wpid = NULL;
    const guint8 args_data[] = { 0x20 + devnumber, 0x00, 0x00 };
    guint8 *result;
    gsize result_length;
    solaar_receiver_read_register (receiver,
                                   SOLAAR_RECEIVER_ID,
                                   SOLAAR_REGISTER_RECEIVER_INFO,
                                   args_data,
                                   3,
                                   &result,
                                   &result_length,
                                   &error);
    if (error != NULL) {
      g_critical ("%s", error->message);
      return NULL;
    }

    if (result_length < 4)
      return NULL;

    wpid = g_strdup_printf ("%02X%02X", result[2], result[3]);
    //guint8 kind = result[6] & 0x0F;

    const SolaarDeviceDescriptor *desc = solaar_device_descriptors_from_wpid (wpid);
    if (desc == NULL) {
      g_critical ("Device wpid `%s' is not supported", wpid);
      return NULL;
    }

    device = g_object_new (SOLAAR_TYPE_DEVICE,
      "receiver", receiver,
      NULL);

    device->devnumber = devnumber;
    device->wpid = g_steal_pointer (&wpid);
    device->descriptor = desc;
    device->polling_rate = result[1];

    return g_steal_pointer (&device);
}

static void
solaar_device_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  SolaarDevice *self = SOLAAR_DEVICE (object);

  switch (prop_id) {
    case PROP_RECEIVER:
      g_value_set_object (value, self->receiver);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
solaar_device_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  SolaarDevice *self = SOLAAR_DEVICE (object);

  switch (prop_id) {
    case PROP_RECEIVER:
      g_clear_object (&self->receiver);
      self->receiver = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
solaar_device_finalize (GObject *gobject)
{
  SolaarDevice *self = SOLAAR_DEVICE (gobject);

  g_clear_object (&self->receiver);
  g_clear_pointer (&self->wpid, g_free);
  g_clear_pointer (&self->serial, g_free);

  G_OBJECT_CLASS (solaar_device_parent_class)->finalize (gobject);
}

static void
solaar_device_class_init (SolaarDeviceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = solaar_device_set_property;
  object_class->get_property = solaar_device_get_property;
  object_class->finalize = solaar_device_finalize;

  obj_properties[PROP_RECEIVER] =
    g_param_spec_object ("receiver",
                         "Receiver",
                         "The parent reciever.",
                         SOLAAR_TYPE_RECEIVER,
                         G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

  g_object_class_install_properties (object_class,
                                     N_PROPERTIES,
                                     obj_properties);
}

static void
solaar_device_init (SolaarDevice *self)
{
}

/**
 * solaar_device_get_receiver:
 * @self: a #SolaarDevice
 * 
 * Get #SolaarReceiver holding this device.
 * 
 * Returns: (transfer full): the #SolaarReceiver linked to this device.
 **/
SolaarReceiver *
solaar_device_get_receiver (SolaarDevice *self)
{
  g_return_val_if_fail (SOLAAR_IS_DEVICE(self), NULL);

  return SOLAAR_RECEIVER (g_object_ref (G_OBJECT (self->receiver)));
}

const gchar *
solaar_device_get_name (SolaarDevice *self)
{
  g_return_val_if_fail (SOLAAR_IS_DEVICE(self), NULL);

  return _(self->descriptor->name);
}

const gchar *
solaar_device_get_code_name (SolaarDevice *self)
{
  g_return_val_if_fail (SOLAAR_IS_DEVICE(self), NULL);

  return self->descriptor->codename;
}

const gchar *
solaar_device_get_serial (SolaarDevice *self)
{
  const guint8 args_data[] = { 0x30 + self->devnumber, 0x00, 0x00 };
  g_autoptr(GError) error = NULL;
  guint8 *result;
  gsize result_length;

  g_return_val_if_fail (SOLAAR_IS_DEVICE(self), NULL);

  if (self->serial != NULL)
    return self->serial;

  solaar_receiver_read_register (SOLAAR_RECEIVER (self->receiver),
                                 SOLAAR_RECEIVER_ID,
                                 SOLAAR_REGISTER_RECEIVER_INFO,
                                 args_data,
                                 3,
                                 &result,
                                 &result_length,
                                 &error);
  if (error != NULL) {
    g_critical ("%s", error->message);
    return 0;
  }

  if (result_length < 4)
    return NULL;

  self->serial = g_strdup_printf ("%02X%02X%02X%02X",
                                  result[0],
                                  result[1],
                                  result[2],
                                  result[3]);


  return self->serial;
}

/**
 * solaar_device_get_icon:
 * @self: a #SolaarDevice
 * 
 * Get the #GIcon representing this device according to its type.
 * 
 * Returns: (transfer full): the #GIcon representing this device.
 **/
GIcon *
solaar_device_get_icon (SolaarDevice *self) {
  g_autoptr(GIcon) icon = NULL;

  g_return_val_if_fail (SOLAAR_IS_DEVICE(self), NULL);

  switch (self->descriptor->kind) {
    case SOLAAR_DEVICE_KIND_KEYBOARD:
      icon = g_themed_icon_new_with_default_fallbacks ("input-keyboard");
      break;
    case SOLAAR_DEVICE_KIND_NUMPAD:
      return g_themed_icon_new_with_default_fallbacks ("input-keyboard");
      g_themed_icon_prepend_name (G_THEMED_ICON (icon), "input-dialpad");
      break;
    case SOLAAR_DEVICE_KIND_TOUCHPAD:
      icon = g_themed_icon_new_with_default_fallbacks ("input-mouse");
      g_themed_icon_prepend_name (G_THEMED_ICON (icon), "input-tablet");
      break;
    default:
      icon = g_themed_icon_new_with_default_fallbacks ("input-mouse");
      break;
  }

  return g_steal_pointer (&icon);
}

void
solaar_device_unpair (SolaarDevice *self)
{
  const guint8 args_data[] = { 0x03, self->devnumber, 0x00 };
  g_autoptr(GError) error = NULL;
  guint8 *result;
  gsize result_length;

  g_return_if_fail (SOLAAR_IS_DEVICE(self));

  solaar_receiver_write_register (SOLAAR_RECEIVER (self->receiver),
                                  SOLAAR_RECEIVER_ID,
                                  SOLAAR_REGISTER_RECEIVER_PAIRING,
                                  args_data,
                                  3,
                                  &result,
                                  &result_length,
                                  &error);
  if (error != NULL) {
    g_critical ("%s", error->message);
  }
}
