/*
 * Copyright 2018 Collabora Ltd. (https://collabora.com)
 *
 * Authored by: Corentin Noël <corentin.noel@collabora.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef __SOLAAR_RECEIVER_H__
#define __SOLAAR_RECEIVER_H__

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define SOLAAR_TYPE_RECEIVER solaar_receiver_get_type ()

typedef struct _SolaarDevice SolaarDevice;

G_DECLARE_FINAL_TYPE (SolaarReceiver, solaar_receiver, SOLAAR, RECEIVER, GObject)

GPtrArray   *solaar_receiver_list             (void);

const gchar *solaar_receiver_get_serial       (SolaarReceiver *self);
const gchar *solaar_receiver_get_name         (SolaarReceiver *self);
const gchar *solaar_receiver_get_path         (SolaarReceiver *self);
guint8       solaar_receiver_get_count        (SolaarReceiver *self);
GPtrArray   *solaar_receiver_get_devices      (SolaarReceiver *self);
gboolean     solaar_receiver_get_pairing_lock (SolaarReceiver *self);
void         solaar_receiver_set_pairing_lock (SolaarReceiver *self,
                                               gboolean        locked);

G_END_DECLS

#endif /* __SOLAAR_RECEIVER_H__ */
