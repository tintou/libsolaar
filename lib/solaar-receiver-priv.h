/*
 * Copyright 2018 Collabora Ltd. (https://collabora.com)
 *
 * Authored by: Corentin Noël <corentin.noel@collabora.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef __SOLAAR_RECEIVER_PRIV_H__
#define __SOLAAR_RECEIVER_PRIV_H__

#include <glib.h>
#include <glib-object.h>

#include "solaar-receiver.h"
#include "solaar-enums.h"
#include "solaar-constants.h"

G_BEGIN_DECLS

gboolean
solaar_receiver_read_register (SolaarReceiver *self,
                               guint8          devnumber,
                               SolaarRegister  reg,
                               const guint8   *args_data,
                               gsize           args_data_length,
                               guint8        **result,
                               gsize          *result_length,
                               GError        **error);

gboolean
solaar_receiver_write_register (SolaarReceiver *self,
                                guint8          devnumber,
                                SolaarRegister  reg,
                                const guint8   *args_data,
                                gsize           args_data_length,
                                guint8        **result,
                                gsize          *result_length,
                                GError        **error);

G_END_DECLS

#endif /* __SOLAAR_RECEIVER_PRIV_H__ */
