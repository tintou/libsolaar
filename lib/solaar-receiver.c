/*
 * Copyright 2018 Collabora Ltd. (https://collabora.com)
 *
 * Authored by: Corentin Noël <corentin.noel@collabora.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#define G_LOG_DOMAIN "solaar-receiver"

#include <glib.h>
#include <gio/gio.h>
#include <gudev/gudev.h>

#include "solaar-receiver.h"
#include "solaar-enums.h"
#include "solaar-device.h"
#include "solaar-device-priv.h"
#include "solaar-constants.h"

struct _SolaarReceiver
{
  GObject parent_instance;
  GUdevDevice *gudev_device;
  GFileIOStream *io_stream;
  int product_id;
  gchar *serial;
  int max_devices;
  GCancellable *cancellable;
};

G_DEFINE_TYPE (SolaarReceiver, solaar_receiver, G_TYPE_OBJECT)

typedef struct {
  int product_id;
} SolaarUnifyingReceiver;

typedef struct {
  int product_id;
} SolaarNanoReceiver;

enum
{
  PROP_GUDEV_DEVICE = 1,
  PROP_PRODUCT_ID,
  PROP_PAIRING_LOCK,
  N_PROPERTIES
};

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL, };

// Common Unifying receivers (with the orange Unifying logo)
const SolaarUnifyingReceiver _unifying_receivers[] = {
  { 0xc52b },
  { 0xc532 },
};

const SolaarNanoReceiver _nano_receivers[] = {
// Nano receviers supporting the Unifying protocol
  { 0xc52f },
// Nano receivers not supporting the Unifying protocol
  { 0xc517 },
  { 0xc518 },
  { 0xc51a },
  { 0xc51b },
  { 0xc521 },
  { 0xc525 },
  { 0xc526 },
  { 0xc52e },
  { 0xc531 },
  { 0xc534 }
};

static gboolean
_solaar_receiver_is_supported (int product_id) {
  for (int i = 0; i < sizeof(_unifying_receivers); i++) {
    if (product_id == _unifying_receivers[i].product_id)
      return TRUE;
  }

  for (int i = 0; i < sizeof(_nano_receivers); i++) {
    if (product_id == _nano_receivers[i].product_id)
      return TRUE;
  }

  return FALSE;
}

static gboolean
_solaar_receiver_read (SolaarReceiver  *self,
                       guint8          *devnumber,
                       guint8          *report_id,
                       guint8         **data,
                       gsize           *data_length,
                       GError         **error)
{
  GInputStream *input_stream = NULL;
  g_autoptr(GByteArray) byte_array = NULL;
  guint8 buffer[_MAX_READ_SIZE];
  gsize bytes_read;

  input_stream = g_io_stream_get_input_stream (G_IO_STREAM (self->io_stream));

  bytes_read = g_input_stream_read (input_stream,
                                    buffer,
                                    _MAX_READ_SIZE,
                                    self->cancellable,
                                    error);
  if (error != NULL && *error != NULL) {
    g_critical ("%s", (*error)->message);
    return FALSE;
  }

  if (report_id != NULL)
    *report_id = buffer[0];

  if (devnumber != NULL)
    *devnumber = buffer[1];

  *data_length = bytes_read - 2;
  byte_array = g_byte_array_sized_new (*data_length);
  byte_array = g_byte_array_append (byte_array, buffer+2, *data_length);
  *data = g_byte_array_free (g_steal_pointer (&byte_array), FALSE);
  return TRUE;
}

gboolean
_solaar_query_register (SolaarReceiver *self,
                        guint8          devnumber,
                        guint16         request_id,
                        const guint8   *args_data,
                        gsize           args_data_length,
                        guint8        **result,
                        gsize          *result_length,
                        GError        **error)
{
  g_autoptr(GByteArray) array = NULL;
  g_autoptr(GBytes) bytes = NULL;
  const guint8 *bytes_data;
  gsize bytes_size;
  GOutputStream *output_stream = NULL;
  gsize bytes_written = 0;
  guint8 received_devnumber;
  guint8 report_id;
  guint8 *data;
  gsize data_length;

  *result = NULL;
  *result_length = 0;

  g_assert (SOLAAR_IS_RECEIVER (self));

  if (args_data_length > _SHORT_MESSAGE_SIZE - 2 || args_data[0] == 0x82) {
    const guint8 wdata[] = { 0x11, devnumber };
    array = g_byte_array_sized_new (20);
    array = g_byte_array_append (array, wdata, 2);
  } else {
    const guint8 wdata[] = { 0x10, devnumber };
    array = g_byte_array_sized_new (7);
    array = g_byte_array_append (array, wdata, 2);
  }

  array = g_byte_array_append (array, (const guint8 *)&request_id, 2);
  array = g_byte_array_append (array, args_data, args_data_length);
  bytes = g_byte_array_free_to_bytes (g_steal_pointer (&array));

  output_stream = g_io_stream_get_output_stream (G_IO_STREAM (self->io_stream));

  bytes_data = g_bytes_get_data (bytes, &bytes_size);
  g_output_stream_write_all (output_stream,
                             bytes_data,
                             bytes_size,
                             &bytes_written,
                             self->cancellable,
                             error);
  if (error != NULL && *error != NULL) {
    g_critical ("WRITE: %s", (*error)->message);
    return FALSE;
  }

  _solaar_receiver_read (self,
                         &received_devnumber,
                         &report_id,
                         &data,
                         &data_length,
                         error);
  if (error != NULL && *error != NULL) {
    g_critical ("READ: %s", (*error)->message);
    return FALSE;
  }

  if (received_devnumber == devnumber) {
    if (report_id == 0x10 && data[0] == 0x8F && data[1] == args_data[2] && data[2] == args_data[3]) {
      guint8 error_id = data[3];
      g_critical ("Error: %02X (HIDPP10)", error_id);
    } else if (data[0] == 0xFF && data[1] == args_data[2] && data[2] == args_data[3]) {
      guint8 error_id = data[3];
      g_critical ("Error: %02X (HIDPP20)", error_id);
    } else {
      *result = data + 3;
      *result_length = data_length - 3;
    }
  }

  return TRUE;
}

gboolean
solaar_receiver_read_register (SolaarReceiver *self,
                               guint8          devnumber,
                               SolaarRegister  reg,
                               const guint8   *args_data,
                               gsize           args_data_length,
                               guint8        **result,
                               gsize          *result_length,
                               GError        **error)
{
  // We need to be sure to use the network endian there.
  guint16 request_id = GUINT16_TO_BE(SOLAAR_REGISTER_READ_COMMAND | (reg & SOLAAR_REGISTER_MASK));
  return _solaar_query_register (self,
                                 devnumber,
                                 request_id,
                                 args_data,
                                 args_data_length,
                                 result,
                                 result_length,
                                 error);
}

gboolean
solaar_receiver_write_register (SolaarReceiver *self,
                                guint8          devnumber,
                                SolaarRegister  reg,
                                const guint8   *args_data,
                                gsize           args_data_length,
                                guint8        **result,
                                gsize          *result_length,
                                GError        **error)
{
  // We need to be sure to use the network endian there.
  guint16 request_id = GUINT16_TO_BE(SOLAAR_REGISTER_WRITE_COMMAND | (reg & SOLAAR_REGISTER_MASK));
  return _solaar_query_register (self,
                                 devnumber,
                                 request_id,
                                 args_data,
                                 args_data_length,
                                 result,
                                 result_length,
                                 error);
}

static gboolean
_solaar_receiver_open (SolaarReceiver *self, GError **error)
{
  g_autoptr(GFile) file = NULL;

  g_assert (SOLAAR_IS_RECEIVER (self));

  file = g_file_new_for_path (g_udev_device_get_device_file (self->gudev_device));
  self->io_stream = g_file_open_readwrite (file, self->cancellable, error);
  if (self->io_stream == NULL)
    return FALSE;

  if (self->product_id != 0xc534) {
    const guint8 args_data[] = { 0x03, 0x00, 0x00 };
    guint8 *result;
    gsize result_length;
    solaar_receiver_read_register (self,
                                   SOLAAR_RECEIVER_ID,
                                   SOLAAR_REGISTER_RECEIVER_INFO,
                                   args_data,
                                   3,
                                   &result,
                                   &result_length,
                                   error);
    if (result_length < 6) {
      self->serial = NULL;
      self->max_devices = 6;
      return TRUE;
    }

    self->serial = g_strdup_printf ("%02X%02X%02X%02X",
                                    result[0],
                                    result[1],
                                    result[2],
                                    result[3]);
    self->max_devices = result[5];
  } else {
    self->serial = NULL;
    self->max_devices = 6;
  }

  return TRUE;
}

static void
solaar_receiver_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  SolaarReceiver *self = SOLAAR_RECEIVER (object);

  switch (prop_id) {
    case PROP_GUDEV_DEVICE:
      g_value_set_object (value, self->gudev_device);
      break;

    case PROP_PRODUCT_ID:
      g_value_set_int (value, self->product_id);
      break;

    case PROP_PAIRING_LOCK:
      g_value_set_boolean (value, solaar_receiver_get_pairing_lock (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
solaar_receiver_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  SolaarReceiver *self = SOLAAR_RECEIVER (object);

  switch (prop_id) {
    case PROP_GUDEV_DEVICE:
      g_clear_object (&self->gudev_device);
      self->gudev_device = g_value_dup_object (value);
      break;

    case PROP_PRODUCT_ID:
      self->product_id = g_value_get_int (value);
      break;

    case PROP_PAIRING_LOCK:
      solaar_receiver_set_pairing_lock (self, g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
solaar_receiver_finalize (GObject *gobject)
{
  SolaarReceiver *self = SOLAAR_RECEIVER (gobject);

  g_clear_object (&self->gudev_device);

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);

  g_clear_object (&self->io_stream);

  G_OBJECT_CLASS (solaar_receiver_parent_class)->finalize (gobject);
}

static void
solaar_receiver_class_init (SolaarReceiverClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = solaar_receiver_set_property;
  object_class->get_property = solaar_receiver_get_property;
  object_class->finalize = solaar_receiver_finalize;

  obj_properties[PROP_GUDEV_DEVICE] =
    g_param_spec_object ("gudev-device",
                         "GUdev Device",
                         "The GUdev Device of the receiver.",
                         G_UDEV_TYPE_DEVICE,
                         G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_PRIVATE);

  obj_properties[PROP_PRODUCT_ID] =
    g_param_spec_int ("product-id",
                      "Product ID",
                      "The Product ID of the receiver.",
                      G_MININT,
                      G_MAXINT,
                      0,
                      G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

  obj_properties[PROP_PAIRING_LOCK] =
    g_param_spec_boolean ("pairing-lock",
                          "Pairing Lock",
                          "Whether pairing a new device is prevented.",
                          FALSE,
                          G_PARAM_READWRITE);

  g_object_class_install_properties (object_class,
                                     N_PROPERTIES,
                                     obj_properties);
}

static void
solaar_receiver_init (SolaarReceiver *self)
{
  self->cancellable = g_cancellable_new ();
}

const gchar *
solaar_receiver_get_serial (SolaarReceiver *self)
{
  g_return_val_if_fail (SOLAAR_IS_RECEIVER(self), NULL);

  return self->serial;
}

const gchar *
solaar_receiver_get_name (SolaarReceiver *self)
{
  g_return_val_if_fail (SOLAAR_IS_RECEIVER(self), NULL);

  if (self->max_devices == 6) {
    return "Unifying Receiver";
  } else if (self->max_devices < 6) {
    return "Nano Receiver";
  } else {
    return NULL;
  }
}

const gchar *
solaar_receiver_get_path (SolaarReceiver *self)
{
  g_return_val_if_fail (SOLAAR_IS_RECEIVER(self), NULL);

  return g_udev_device_get_device_file (self->gudev_device);
}

guint8
solaar_receiver_get_count (SolaarReceiver *self)
{
  g_autoptr(GError) error = NULL;

  g_return_val_if_fail (SOLAAR_IS_RECEIVER(self), 0);

  const guint8 args_data[] = { 0x00, 0x00, 0x00 };
  guint8 *result;
  gsize result_length;
  solaar_receiver_read_register (self,
                                 SOLAAR_RECEIVER_ID,
                                 SOLAAR_REGISTER_RECEIVER_CONNECTION,
                                 args_data,
                                 3,
                                 &result,
                                 &result_length,
                                 &error);
  if (error != NULL) {
    g_critical ("%s", error->message);
    return 0;
  }

  if (result_length < 1)
    return 0;

  return result[0];
}

/**
 * solaar_receiver_get_devices:
 * @self: a #SolaarReceiver
 * 
 * Get all the #SolaarDevice paired with this receiver.
 * 
 * Returns: (transfer full) (element-type SolaarDevice): the list of all
 * paired #SolaarDevice.
 **/
GPtrArray *
solaar_receiver_get_devices (SolaarReceiver *self)
{
  g_autoptr(GPtrArray) result = g_ptr_array_new ();

  guint8 count = solaar_receiver_get_count (self);
  for (guint8 i = 0; i < count; i++) {
    g_autoptr(SolaarDevice) dev = _solaar_device_new (self, i);

    if (dev != NULL) {
        g_ptr_array_add (result, g_steal_pointer (&dev));
    }
  }

  return g_steal_pointer (&result);
}

gboolean
solaar_receiver_get_pairing_lock (SolaarReceiver *self)
{
  g_autoptr(GError) error = NULL;

  g_return_val_if_fail (SOLAAR_IS_RECEIVER(self), FALSE);

  const guint8 args_data[] = { 0x01, 0x00, 0x00 };
  guint8 *result;
  gsize result_length;
  solaar_receiver_read_register (self,
                                 SOLAAR_RECEIVER_ID,
                                 SOLAAR_REGISTER_RECEIVER_PAIRING,
                                 args_data,
                                 3,
                                 &result,
                                 &result_length,
                                 &error);
  if (error != NULL) {
    g_critical ("%s", error->message);
    return FALSE;
  }

  for (int i = 0; i < result_length; i++) {
    g_critical ("%02X", result[i]);
  }
  return result[0] == 0x02;
}

void
solaar_receiver_set_pairing_lock (SolaarReceiver *self,
                                  gboolean locked)
{
  g_autoptr(GError) error = NULL;
  const guint8 args_data[] = { locked ? 0x02 : 0x01, 0x00, 0x00 };
  guint8 *result;
  gsize result_length;

  g_return_if_fail (SOLAAR_IS_RECEIVER(self));

  solaar_receiver_write_register (self,
                                  SOLAAR_RECEIVER_ID,
                                  SOLAAR_REGISTER_RECEIVER_PAIRING,
                                  args_data,
                                  3,
                                  &result,
                                  &result_length,
                                  &error);

  if (error != NULL) {
    g_critical ("%s", error->message);
    return;
  }
}

/**
 * solaar_receiver_list:
 * 
 * Get all the #SolaarReceiver available in the system.
 * 
 * Returns: (transfer full) (element-type SolaarReceiver): the list of all
 * available #SolaarReceiver in the system
 **/
GPtrArray *
solaar_receiver_list (void)
{
  g_autoptr(GUdevClient) client = NULL;
  g_autolist(GUdevDevice) subsystems = NULL;
  GList *l = NULL;
  g_autoptr(GPtrArray) result = g_ptr_array_new ();

  client = g_udev_client_new (NULL);
  subsystems = g_udev_client_query_by_subsystem (client, "hidraw");
  for (l = subsystems; l != NULL; l = l->next) {
    GUdevDevice *device = l->data;
    g_autoptr(GUdevDevice) usb_device = NULL;
    g_autoptr(GUdevDevice) hid_device = NULL;
    g_autoptr(GUdevDevice) intf_device = NULL;
    g_autoptr(SolaarReceiver) receiver = NULL;
    g_autoptr(GError) error = NULL;
    const gchar *hid_driver_name;
    const gchar *usb_interface;
    const gchar *vid_s, *pid_s;
    int vid, pid;

    usb_device = g_udev_device_get_parent_with_subsystem (device, "usb", "usb_device");
    if (usb_device == NULL)
      continue;

    vid_s = g_udev_device_get_property (usb_device, "ID_VENDOR_ID");
    vid = g_ascii_strtoll (vid_s, NULL, 16);
    pid_s = g_udev_device_get_property (usb_device, "ID_MODEL_ID");
    pid = g_ascii_strtoll (pid_s, NULL, 16);
    if (vid != 0x046d || !_solaar_receiver_is_supported (pid))
      continue;

    hid_device = g_udev_device_get_parent_with_subsystem (device, "hid", NULL);
    if (hid_device == NULL)
      continue;

    hid_driver_name = g_udev_device_get_property (hid_device, "DRIVER");
    if (g_strcmp0 (hid_driver_name, "logitech-djreceiver") != 0 &&
        g_strcmp0 (hid_driver_name, "hid-generic") != 0 &&
        g_strcmp0 (hid_driver_name, "generic-usb") != 0)
        continue;

    intf_device = g_udev_device_get_parent_with_subsystem (device, "usb", "usb_interface");
    if (intf_device == NULL) {
      usb_interface = g_udev_device_get_sysfs_attr (intf_device, "bInterfaceNumber");
      g_critical ("%s", usb_interface);
    }

    receiver = g_object_new (SOLAAR_TYPE_RECEIVER,
      "gudev-device", device,
      "product-id", pid,
      NULL);

    if (_solaar_receiver_open (receiver, &error)) {
        g_ptr_array_add (result, g_steal_pointer (&receiver));
    } else {
        g_critical ("Failed to open receiver `%s': %s", g_udev_device_get_name (device), error->message);
    }
  }

  return g_steal_pointer (&result);
}
