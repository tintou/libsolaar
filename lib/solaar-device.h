/*
 * Copyright 2018 Collabora Ltd. (https://collabora.com)
 *
 * Authored by: Corentin Noël <corentin.noel@collabora.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef __SOLAAR_DEVICE_H__
#define __SOLAAR_DEVICE_H__

#include <glib.h>
#include <gio/gio.h>
#include <glib-object.h>

#include "solaar-receiver.h"

G_BEGIN_DECLS

#define SOLAAR_TYPE_DEVICE solaar_device_get_type ()

G_DECLARE_FINAL_TYPE (SolaarDevice, solaar_device, SOLAAR, DEVICE, GObject)

SolaarReceiver *solaar_device_get_receiver  (SolaarDevice *self);
const gchar    *solaar_device_get_name      (SolaarDevice *self);
const gchar    *solaar_device_get_code_name (SolaarDevice *self);
const gchar    *solaar_device_get_serial    (SolaarDevice *self);
void            solaar_device_unpair        (SolaarDevice *self);
GIcon          *solaar_device_get_icon      (SolaarDevice *self);

G_END_DECLS

#endif /* __SOLAAR_DEVICE_H__ */
