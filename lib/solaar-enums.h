/*
 * Copyright 2018 Collabora Ltd. (https://collabora.com)
 *
 * Authored by: Corentin Noël <corentin.noel@collabora.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef __SOLAAR_ENUMS_H__
#define __SOLAAR_ENUMS_H__

/**
 * SolaarDeviceKind:
 **/
typedef enum {
	SOLAAR_DEVICE_KIND_KEYBOARD = 0x00,
	SOLAAR_DEVICE_KIND_REMOTE_CONTROL = 0x01,
	SOLAAR_DEVICE_KIND_NUMPAD = 0x02,
	SOLAAR_DEVICE_KIND_MOUSE = 0x03,
	SOLAAR_DEVICE_KIND_TOUCHPAD = 0x04,
	SOLAAR_DEVICE_KIND_TRACKBALL = 0x05,
	SOLAAR_DEVICE_KIND_PRESENTER = 0x06,
	SOLAAR_DEVICE_KIND_RECEIVER = 0x07
} SolaarDeviceKind;

/**
 * SolaarProtocolVersion:
 **/
typedef enum {
	SOLAAR_PROTOCOL_VERSION_1_0 = 0,
	SOLAAR_PROTOCOL_VERSION_2_0 = 1,
	SOLAAR_PROTOCOL_VERSION_4_5 = 2,
} SolaarProtocolVersion;

#endif /* __SOLAAR_ENUMS_H__ */
