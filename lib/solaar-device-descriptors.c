/*
 * Copyright 2018 Collabora Ltd. (https://collabora.com)
 *
 * Authored by: Corentin Noël <corentin.noel@collabora.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#define G_LOG_DOMAIN "solaar-device-descriptors"

#include <glib/gi18n.h>

#include "solaar-enums.h"
#include "solaar-device-descriptors.h"

static GData *
_solaar_device_descriptors_get_datalist (void) {
  static GData *datalist = NULL;
  GQuark desc_quark;

  if (datalist != NULL)
    return datalist;

  g_datalist_init (&datalist);

  static const SolaarDeviceDescriptor _1003_desc = {
    .name=N_("V400 Laser Cordless Mouse"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="V400",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("1003");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1003_desc);

  static const SolaarDeviceDescriptor _4041_desc = {
    .name=N_("Wireless Mouse MX Master"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="MX Master",
    .protocol=SOLAAR_PROTOCOL_VERSION_4_5,
  };
  desc_quark = g_quark_from_static_string ("4041");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4041_desc);

  static const SolaarDeviceDescriptor _1004_desc = {
    .name=N_("MX610 Left-Handled Mouse"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="MX610L",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("1004");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1004_desc);

  static const SolaarDeviceDescriptor _2008_desc = {
    .name=N_("Wireless Keyboard MK700"),
    .kind=SOLAAR_DEVICE_KIND_KEYBOARD,
    .codename="MK700",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("2008");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_2008_desc);

  static const SolaarDeviceDescriptor _4069_desc = {
    .name=N_("Wireless Mouse MX Master 2S"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="MX Master 2S",
    .protocol=SOLAAR_PROTOCOL_VERSION_4_5,
  };
  desc_quark = g_quark_from_static_string ("4069");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4069_desc);

  static const SolaarDeviceDescriptor _4022_desc = {
    .name=N_("Wireless Mouse M150"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M150",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("4022");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4022_desc);

  static const SolaarDeviceDescriptor _400E_4024_desc = {
    .name=N_("Wireless Touch Keyboard K400"),
    .kind=SOLAAR_DEVICE_KIND_KEYBOARD,
    .codename="K400",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("400E");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_400E_4024_desc);
  desc_quark = g_quark_from_static_string ("4024");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_400E_4024_desc);
  static const SolaarDeviceDescriptor _4061_desc = {
    .name=N_("Wireless Keyboard K375s"),
    .kind=SOLAAR_DEVICE_KIND_KEYBOARD,
    .codename="K375s",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("4061");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4061_desc);

  static const SolaarDeviceDescriptor _4023_desc = {
    .name=N_("Wireless Keyboard MK270"),
    .kind=SOLAAR_DEVICE_KIND_KEYBOARD,
    .codename="MK270",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("4023");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4023_desc);

  static const SolaarDeviceDescriptor _1002_desc = {
    .name=N_("G7 Cordless Laser Mouse"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="G7",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("1002");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1002_desc);

  static const SolaarDeviceDescriptor _101D_desc = {
    .name=N_("Wireless Mouse M505"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M505/B605",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("101D");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_101D_desc);

  static const SolaarDeviceDescriptor _1023_desc = {
    .name=N_("G700 Gaming Mouse"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="G700",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("1023");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1023_desc);

  static const SolaarDeviceDescriptor _101A_desc = {
    .name=N_("Performance Mouse MX"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="Performance MX",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("101A");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_101A_desc);

  static const SolaarDeviceDescriptor _2011_desc = {
    .name=N_("Wireless Keyboard K520"),
    .kind=SOLAAR_DEVICE_KIND_KEYBOARD,
    .codename="K520",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("2011");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_2011_desc);

  static const SolaarDeviceDescriptor _2010_desc = {
    .name=N_("Wireless Illuminated Keyboard K800"),
    .kind=SOLAAR_DEVICE_KIND_KEYBOARD,
    .codename="K800",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("2010");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_2010_desc);

  static const SolaarDeviceDescriptor _4051_desc = {
    .name=N_("Wireless Mouse M510"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M510v2",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("4051");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4051_desc);

  static const SolaarDeviceDescriptor _102A_desc = {
    .name=N_("G700s Gaming Mouse"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="G700s",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("102A");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_102A_desc);

  static const SolaarDeviceDescriptor _4032_desc = {
    .name=N_("Illuminated Living-Room Keyboard K830"),
    .kind=SOLAAR_DEVICE_KIND_KEYBOARD,
    .codename="K830",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("4032");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4032_desc);

  static const SolaarDeviceDescriptor _404D_desc = {
    .name=N_("Wireless Touch Keyboard K400 Plus"),
    .kind=SOLAAR_DEVICE_KIND_KEYBOARD,
    .codename="Plus",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("404D");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_404D_desc);

  static const SolaarDeviceDescriptor _4038_desc = {
    .name=N_("Wireless Mouse M185"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M185",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("4038");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4038_desc);

  static const SolaarDeviceDescriptor _4011_desc = {
    .name=N_("Wireless Touchpad"),
    .kind=SOLAAR_DEVICE_KIND_TOUCHPAD,
    .codename="Wireless Touch",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("4011");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4011_desc);

  static const SolaarDeviceDescriptor _1006_100D_desc = {
    .name=N_("VX Revolution"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="VX Revolution",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("1006");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1006_100D_desc);
  desc_quark = g_quark_from_static_string ("100D");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1006_100D_desc);
  static const SolaarDeviceDescriptor _101B_desc = {
    .name=N_("Marathon Mouse M705 (M-R0009)"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M705 (M-R0009)",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("101B");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_101B_desc);

  static const SolaarDeviceDescriptor _1007_100E_desc = {
    .name=N_("MX Air"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="MX Air",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("1007");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1007_100E_desc);
  desc_quark = g_quark_from_static_string ("100E");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1007_100E_desc);
  static const SolaarDeviceDescriptor _1008_100C_desc = {
    .name=N_("MX Revolution"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="MX Revolution",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("1008");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1008_100C_desc);
  desc_quark = g_quark_from_static_string ("100C");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1008_100C_desc);
  static const SolaarDeviceDescriptor _404A_desc = {
    .name=N_("Anywhere Mouse MX 2"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="Anywhere MX 2",
    .protocol=SOLAAR_PROTOCOL_VERSION_4_5,
  };
  desc_quark = g_quark_from_static_string ("404A");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_404A_desc);

  static const SolaarDeviceDescriptor _100A_1016_desc = {
    .name=N_("MX620 Laser Cordless Mouse"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="MX620",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("100A");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_100A_1016_desc);
  desc_quark = g_quark_from_static_string ("1016");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_100A_1016_desc);
  static const SolaarDeviceDescriptor _4002_desc = {
    .name=N_("Wireless Solar Keyboard K750"),
    .kind=SOLAAR_DEVICE_KIND_KEYBOARD,
    .codename="K750",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("4002");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4002_desc);

  static const SolaarDeviceDescriptor _101F_desc = {
    .name=N_("Wireless Mouse M305"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M305",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("101F");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_101F_desc);

  static const SolaarDeviceDescriptor _200F_desc = {
    .name=N_("Wireless Keyboard MK320"),
    .kind=SOLAAR_DEVICE_KIND_KEYBOARD,
    .codename="MK320",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("200F");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_200F_desc);

  static const SolaarDeviceDescriptor _4004_desc = {
    .name=N_("Wireless Keyboard K360"),
    .kind=SOLAAR_DEVICE_KIND_KEYBOARD,
    .codename="K360",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("4004");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4004_desc);

  static const SolaarDeviceDescriptor _4101_desc = {
    .name=N_("Wireless Rechargeable Touchpad T650"),
    .kind=SOLAAR_DEVICE_KIND_TOUCHPAD,
    .codename="T650",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("4101");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4101_desc);

  static const SolaarDeviceDescriptor _1014_desc = {
    .name=N_("MX 1100 Cordless Laser Mouse"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="MX 1100",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("1014");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1014_desc);

  static const SolaarDeviceDescriptor _1017_desc = {
    .name=N_("Anywhere Mouse MX"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="Anywhere MX",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("1017");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1017_desc);

  static const SolaarDeviceDescriptor _1011_desc = {
    .name=N_("V450 Nano Cordless Laser Mouse"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="V450 Nano",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("1011");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1011_desc);

  static const SolaarDeviceDescriptor _1013_desc = {
    .name=N_("V550 Nano Cordless Laser Mouse"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="V550 Nano",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("1013");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1013_desc);

  static const SolaarDeviceDescriptor _1001_desc = {
    .name=N_("MX610 Laser Cordless Mouse"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="MX610",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("1001");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1001_desc);

  static const SolaarDeviceDescriptor _101C_desc = {
    .name=N_("Wireless Mouse M350"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M350",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("101C");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_101C_desc);

  static const SolaarDeviceDescriptor _1024_desc = {
    .name=N_("Wireless Mouse M310"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M310",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("1024");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1024_desc);

  static const SolaarDeviceDescriptor _4019_desc = {
    .name=N_("Wireless Mouse M187"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M187",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("4019");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4019_desc);

  static const SolaarDeviceDescriptor _4055_desc = {
    .name=N_("Wireless Mouse M185 old"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M185o",
    .protocol=SOLAAR_PROTOCOL_VERSION_4_5,
  };
  desc_quark = g_quark_from_static_string ("4055");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4055_desc);

  static const SolaarDeviceDescriptor _4054_desc = {
    .name=N_("Wireless Mouse M185 new"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M185n",
    .protocol=SOLAAR_PROTOCOL_VERSION_4_5,
  };
  desc_quark = g_quark_from_static_string ("4054");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4054_desc);

  static const SolaarDeviceDescriptor _100B_100F_desc = {
    .name=N_("VX Nano Cordless Laser Mouse"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="VX Nano",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("100B");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_100B_100F_desc);
  desc_quark = g_quark_from_static_string ("100F");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_100B_100F_desc);
  static const SolaarDeviceDescriptor _1005_desc = {
    .name=N_("V450 Laser Cordless Mouse"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="V450",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("1005");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1005_desc);

  static const SolaarDeviceDescriptor _400A_desc = {
    .name=N_("Wireless Mouse M325"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M325",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("400A");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_400A_desc);

  static const SolaarDeviceDescriptor _1029_desc = {
    .name=N_("Fujitsu Sonic Mouse"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="Sonic",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("1029");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1029_desc);

  static const SolaarDeviceDescriptor _2007_desc = {
    .name=N_("Wireless Compact Keyboard K340"),
    .kind=SOLAAR_DEVICE_KIND_KEYBOARD,
    .codename="K340",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("2007");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_2007_desc);

  static const SolaarDeviceDescriptor _4013_desc = {
    .name=N_("Wireless Mouse M525"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M525",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("4013");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4013_desc);

  static const SolaarDeviceDescriptor _406D_desc = {
    .name=N_("Marathon Mouse M705 (M-R0073)"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M705 (M-R0073)",
    .protocol=SOLAAR_PROTOCOL_VERSION_4_5,
  };
  desc_quark = g_quark_from_static_string ("406D");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_406D_desc);

  static const SolaarDeviceDescriptor _405B_desc = {
    .name=N_("Wireless Solar Keyboard K780"),
    .kind=SOLAAR_DEVICE_KIND_KEYBOARD,
    .codename="K780",
    .protocol=SOLAAR_PROTOCOL_VERSION_4_5,
  };
  desc_quark = g_quark_from_static_string ("405B");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_405B_desc);

  static const SolaarDeviceDescriptor _1025_desc = {
    .name=N_("Wireless Mouse M510"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M510",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("1025");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1025_desc);

  static const SolaarDeviceDescriptor _1020_desc = {
    .name=N_("Wireless Mouse M215"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M215",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("1020");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_1020_desc);

  static const SolaarDeviceDescriptor _401A_desc = {
    .name=N_("Touch Mouse M600"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M600",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("401A");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_401A_desc);

  static const SolaarDeviceDescriptor _4003_desc = {
    .name=N_("Wireless Keyboard K270(unifying)"),
    .kind=SOLAAR_DEVICE_KIND_KEYBOARD,
    .codename="K270(unifying)",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("4003");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4003_desc);

  static const SolaarDeviceDescriptor _200A_desc = {
    .name=N_("Wireless Wave Keyboard K350"),
    .kind=SOLAAR_DEVICE_KIND_KEYBOARD,
    .codename="K350",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("200A");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_200A_desc);

  static const SolaarDeviceDescriptor _4008_desc = {
    .name=N_("Wireless Mouse M175"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M175",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("4008");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4008_desc);

  static const SolaarDeviceDescriptor _4017_desc = {
    .name=N_("Wireless Mouse M345"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M345",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("4017");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4017_desc);

  static const SolaarDeviceDescriptor _4007_desc = {
    .name=N_("Couch Mouse M515"),
    .kind=SOLAAR_DEVICE_KIND_MOUSE,
    .codename="M515",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("4007");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_4007_desc);

  static const SolaarDeviceDescriptor _2006_desc = {
    .name=N_("Number Pad N545"),
    .kind=SOLAAR_DEVICE_KIND_NUMPAD,
    .codename="N545",
    .protocol=SOLAAR_PROTOCOL_VERSION_1_0,
  };
  desc_quark = g_quark_from_static_string ("2006");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_2006_desc);

  static const SolaarDeviceDescriptor _400D_desc = {
    .name=N_("Wireless Keyboard K230"),
    .kind=SOLAAR_DEVICE_KIND_KEYBOARD,
    .codename="K230",
    .protocol=SOLAAR_PROTOCOL_VERSION_2_0,
  };
  desc_quark = g_quark_from_static_string ("400D");
  g_datalist_id_set_data (&datalist, desc_quark, (gpointer)&_400D_desc);

  return datalist;
}

const SolaarDeviceDescriptor *
solaar_device_descriptors_from_wpid (gchar *wpid)
{
  // We need to be sure that the datalist is generated before trying the quark.
  GData *data = _solaar_device_descriptors_get_datalist ();
  GQuark wpid_quark = g_quark_try_string (wpid);
  const SolaarDeviceDescriptor* result = NULL;

  g_return_val_if_fail (wpid != NULL, NULL);
  g_return_val_if_fail (wpid_quark != 0, NULL);

  result = g_datalist_id_get_data (&data, wpid_quark);
  return result;
}

