#include<solaar.h>

void show_device (SolaarDevice *device,
                  int *index)
{

  g_print ("%d: %s [%s:%s]\n",
           *index,
           solaar_device_get_name (device),
           solaar_device_get_code_name (device),
           solaar_device_get_serial (device));
  (*index)++;
}

void show_receiver (SolaarReceiver *receiver,
                    gpointer user_data)
{
  g_autoptr(GPtrArray) devices = NULL;
  g_autoptr(GError) error = NULL;
  int i = 1;

  g_print ("%s [%s:%s] with %d devices\n",
           solaar_receiver_get_name (receiver),
           solaar_receiver_get_path (receiver),
           solaar_receiver_get_serial (receiver),
           solaar_receiver_get_count (receiver));

  devices = solaar_receiver_get_devices (receiver);
  g_ptr_array_set_free_func(devices, (GDestroyNotify) g_object_unref);

  g_ptr_array_foreach (devices, (GFunc) show_device, &i);
}

int main () {
  // Do some stress-test
  for (int i = 0; i < 500; i++) {
    g_autoptr(GPtrArray) receivers = solaar_receiver_list ();
    g_ptr_array_set_free_func(receivers, (GDestroyNotify) g_object_unref);

    g_ptr_array_foreach (receivers, (GFunc) show_receiver, NULL);
  }

  return 0;
}
